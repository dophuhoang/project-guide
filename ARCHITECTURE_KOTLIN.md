﻿# App architecture

We will build an app that implements  [recommended architecture](https://developer.android.com/topic/libraries/architecture/guide.html) using the Android Architecture Components.

## Overview

To start, consider the following diagram, which shows how all the modules should interact with one another after designing the app:

![enter image description here](https://codelabs.developers.google.com/codelabs/android-room-with-a-view-kotlin/img/a7da8f5ea91bac52.png)

**Entity:**  Annotated class that describes a database table when working with  [Room](https://developer.android.com/topic/libraries/architecture/room).

**SQLite database:**  On device storage. Room persistence library creates and maintains this database for you.

**DAO:**  Data access object. A mapping of SQL queries to functions. When you use a DAO, you call the methods, and Room takes care of the rest.

[**Room database**](https://developer.android.com/topic/libraries/architecture/room)**:**  Simplifies database work and serves as an access point to the underlying SQLite database (hides  `SQLiteOpenHelper)`. The Room database uses the DAO to issue queries to the SQLite database.

**Repository:**  Used to manage multiple data sources.

[**ViewModel**](https://developer.android.com/topic/libraries/architecture/viewmodel)**:**  Acts as a communication center between the Repository (data) and the UI. UI no longer needs to worry about origin of the data either. ViewModel instances survive Activity/Fragment recreation.

[**LiveData**](https://developer.android.com/topic/libraries/architecture/livedata)**:** A data holder class that can be  [observed](https://en.wikipedia.org/wiki/Observer_pattern). Always holds/caches latest version of data. Notifies its observers when the data has changed.  `LiveData`  is lifecycle aware. UI components just observe relevant data and don't stop or resume observation. LiveData automatically manages all of this since it's aware of the relevant lifecycle status changes while observing.

## Build the user interface

The UI consists of a fragment,  `UserProfileFragment`, and its corresponding layout file,  `user_profile_layout.xml`.

To drive the UI, our data model needs to hold the following data elements:

-   **User ID**: The identifier for the user. It's best to pass this information into the fragment using the fragment arguments. If the Android OS destroys our process, this information is preserved, so the ID is available the next time our app is restarted.
-   **User object**: A data class that holds details about the user.

We use a  `UserProfileViewModel`, based on the  _ViewModel_  architecture component, to keep this information.

> A  **[`ViewModel`](https://developer.android.com/topic/libraries/architecture/viewmodel)**  object provides the data for a specific UI component, such as a fragment or activity, and contains data-handling business logic to communicate with the model. For example, the  `ViewModel`  can call other components to load the data, and it can forward user requests to modify the data. The  `ViewModel`  doesn't know about UI components, so it isn't affected by configuration changes, such as recreating an activity when rotating the device.

We've now defined the following files:

-   `user_profile.xml`: The UI layout definition for the screen.
-   `UserProfileFragment`: The UI controller that displays the data.
-   `UserProfileViewModel`: The class that prepares the data for viewing in the  `UserProfileFragment`  and reacts to user interactions.

The following code snippets show the starting contents for these files. (The layout file is omitted for simplicity.)

*UserProfileViewModel*

```
class UserProfileViewModel : ViewModel() {   
	val userId : String = TODO()   
	val user : User = TODO()
}
```

*UserProfileFragment*

```
class  UserProfileFragment  :  Fragment()  {  
	// To use the viewModels() extension function, include  
	// "androidx.fragment:fragment-ktx:latest-version" in your app  
	// module's [build.gradle](https://developer.android.com/studio/build#module-level) file.  
	private  val viewModel:  UserProfileViewModel by viewModels()  
	
	override  fun onCreateView( inflater:  LayoutInflater, container:  ViewGroup?, savedInstanceState:  Bundle?  ):  View  {  
		return inflater.inflate(R.layout.main_fragment, container,  false)  
	}  
}  
```

Now that we have these code modules, how do we connect them? After all, when the  `user`  field is set in the  `UserProfileViewModel`  class, we need a way to inform the UI.

To obtain the  `user`, our  `ViewModel`  needs to access the Fragment arguments. We can either pass them from the Fragment, or better, using the  [SavedState module](https://developer.android.com/topic/libraries/architecture/viewmodel-savedstate), we can make our ViewModel read the argument directly:

**Note:** SavedStateHandle allows ViewModel to access the saved state and arguments of the associated Fragment or Activity.

```
// UserProfileViewModel
class UserProfileViewModel(   
	savedStateHandle: SavedStateHandle
) : ViewModel() {   
	val userId : String = savedStateHandle["uid"] ?:          
		throw IllegalArgumentException("missing user id")   
	val user : User = TODO()
}

// UserProfileFragment
private val viewModel: UserProfileViewModel by viewModels(   
	factoryProducer = { SavedStateVMFactory(this) }   
	...
)
```

Now we need to inform our Fragment when the user object is obtained. This is where the  _LiveData_  architecture component comes in.

[LiveData](https://developer.android.com/topic/libraries/architecture/livedata)  is an observable data holder. Other components in your app can monitor changes to objects using this holder without creating explicit and rigid dependency paths between them. The LiveData component also respects the lifecycle state of your app's components—such as activities, fragments, and services—and includes cleanup logic to prevent object leaking and excessive memory consumption.

**Note:** If you're already using a library like  [RxJava](https://github.com/ReactiveX/RxJava), you can continue using them instead of LiveData. When you use libraries and approaches like these, however, make sure you handle your app's lifecycle properly. In particular, make sure to pause your data streams when the related  `LifecycleOwner`  is stopped and to destroy these streams when the related  `LifecycleOwner`  is destroyed. You can also add the  `android.arch.lifecycle:reactivestreams`  artifact to use  `LiveData`  with another reactive streams library, such as RxJava2.

To incorporate the LiveData component into our app, we change the field type in the  `UserProfileViewModel`  to  `LiveData<User>`. Now, the  `UserProfileFragment`  is informed when the data is updated. Furthermore, because this  [`LiveData`](https://developer.android.com/reference/android/arch/lifecycle/LiveData.html)  field is lifecycle aware, it automatically cleans up references after they're no longer needed.

UserProfileViewModel

```
class UserProfileViewModel(   savedStateHandle: SavedStateHandle) : ViewModel() {   val userId : String = savedStateHandle["uid"] ?:          throw IllegalArgumentException("missing user id")   val user : LiveData<User> = TODO()}
```

Now we modify  `UserProfileFragment`  to observe the data and update the UI:

UserProfileFragment

```
override fun onViewCreated(view: View, savedInstanceState: Bundle?) {   super.onViewCreated(view, savedInstanceState)   viewModel.user.observe(viewLifecycleOwner) {       // update UI   }}
```

Every time the user profile data is updated, the  [`onChanged()`](https://developer.android.com/reference/android/arch/lifecycle/Observer.html#onChanged(T))  callback is invoked, and the UI is refreshed.

If you're familiar with other libraries where observable callbacks are used, you might have realized that we didn't override the fragment's  [`onStop()`](https://developer.android.com/reference/android/app/Fragment.html#onStop())  method to stop observing the data. This step isn't necessary with LiveData because it's lifecycle aware, which means it doesn't invoke the  `onChanged()`  callback unless the fragment is in an active state; that is, it has received  [`onStart()`](https://developer.android.com/reference/android/app/Fragment.html#onStart())  but hasn't yet received  [`onStop()`](https://developer.android.com/reference/android/app/Fragment.html#onStop())). LiveData also automatically removes the observer when the fragment's  [`onDestroy()`](https://developer.android.com/reference/android/app/Fragment.html#onDestroy())  method is called.

We also didn't add any logic to handle configuration changes, such as the user rotating the device's screen. The  `UserProfileViewModel`  is automatically restored when the configuration changes, so as soon as the new fragment is created, it receives the same instance of  `ViewModel`, and the callback is invoked immediately using the current data. Given that  `ViewModel`  objects are intended to outlast the corresponding  `View`  objects that they update, you shouldn't include direct references to  `View`  objects within your implementation of  `ViewModel`. For more information about the lifetime of a  `ViewModel`  corresponds to the lifecycle of UI components, see  [The lifecycle of a ViewModel](https://developer.android.com/topic/libraries/architecture/viewmodel.html#lifecycle).

## Fetch data

Now that we've used LiveData to connect the  `UserProfileViewModel`  to the  `UserProfileFragment`, how can we fetch the user profile data?

For this example, we assume that our backend provides a REST API. We use the  [Retrofit](http://square.github.io/retrofit/)  library to access our backend, though you are free to use a different library that serves the same purpose.

Here's our definition of  `Webservice`  that communicates with our backend:

*Webservice*

```
interface Webservice {   
	/**    
	* @GET declares an HTTP GET request
	* @Path("user") annotation on the userId parameter marks it as a    
	* replacement for the {user} placeholder in the @GET path    
	*/
	@GET("/users/{user}")   
	fun getUser(@Path("user") userId: String): Call<User>
}
```

A first idea for implementing the  `ViewModel`  might involve directly calling the  `Webservice`  to fetch the data and assign this data to our  `LiveData`  object. This design works, but by using it, our app becomes more and more difficult to maintain as it grows. It gives too much responsibility to the  `UserProfileViewModel`  class, which violates the  [separation of concerns](https://developer.android.com/jetpack/docs/guide#separation-of-concerns)  principle. Additionally, the scope of a  `ViewModel`  is tied to an  [`Activity`](https://developer.android.com/reference/android/app/Activity.html)  or  [`Fragment`](https://developer.android.com/reference/android/app/Fragment.html)  lifecycle, which means that the data from the  `Webservice`  is lost when the associated UI object's lifecycle ends. This behavior creates an undesirable user experience.

Instead, our ViewModel delegates the data-fetching process to a new module, a  _repository_.

**Repository**  modules handle data operations. They provide a clean API so that the rest of the app can retrieve this data easily. They know where to get the data from and what API calls to make when data is updated. You can consider repositories to be mediators between different data sources, such as persistent models, web services, and caches.

Our  `UserRepository`  class, shown in the following code snippet, uses an instance of  `WebService`  to fetch a user's data:

*UserRepository*

```
class UserRepository {   
	private val webservice: Webservice = TODO()   
	// ...   
	fun getUser(userId: String): LiveData<User> {       
	// This isn't an optimal implementation. We'll fix it later.       
		val data = MutableLiveData<User>()       
		webservice.getUser(userId).enqueue(object : Callback<User> {           override fun onResponse(call: Call<User>, response: Response<User>) {               data.value = response.body()           }           // Error case is left out for brevity.           override fun onFailure(call: Call<User>, t: Throwable) {               TODO()           }       })       return data   }}
```

Even though the repository module looks unnecessary, it serves an important purpose: it abstracts the data sources from the rest of the app. Now, our  `UserProfileViewModel`  doesn't know how the data is fetched, so we can provide the view model with data obtained from several different data-fetching implementations.

**Note:** We've left out the network error case for the sake for simplicity. For an alternative implementation that exposes errors and loading status, see  [Addendum: exposing network status](https://developer.android.com/jetpack/docs/guide#addendum).

### Manage dependencies between components

The  `UserRepository`  class above needs an instance of  `Webservice`  to fetch the user's data. It could simply create the instance, but to do that, it also needs to know the dependencies of the  `Webservice`  class. Additionally,`UserRepository`  is probably not the only class that needs a  `Webservice`. This situation requires us to duplicate code, as each class that needs a reference to  `Webservice`  needs to know how to construct it and its dependencies. If each class creates a new  `WebService`, our app could become very resource heavy.

You can use the following design patterns to address this problem:

-   [Dependency injection (DI)](https://en.wikipedia.org/wiki/Dependency_injection): Dependency injection allows classes to define their dependencies without constructing them. At runtime, another class is responsible for providing these dependencies. We recommend the  [Dagger 2](https://dagger.dev/)  library for implementing dependency injection in Android apps. Dagger 2 automatically constructs objects by walking the dependency tree, and it provides compile-time guarantees on dependencies.
-   [Service locator](https://en.wikipedia.org/wiki/Service_locator_pattern): The service locator pattern provides a registry where classes can obtain their dependencies instead of constructing them.

It's easier to implement a service registry than use DI, so if you aren't familiar with DI, use the service locator pattern instead.

These patterns allow you to scale your code because they provide clear patterns for managing dependencies without duplicating code or adding complexity. Furthermore, these patterns allow you to quickly switch between test and production data-fetching implementations.

Our example app uses  [Dagger 2](https://dagger.dev/)  to manage the  `Webservice`  object's dependencies.

## Connect ViewModel and the repository

Now, we modify our  `UserProfileViewModel`  to use the  `UserRepository`  object:

*UserProfileViewModel*

```
class UserProfileViewModel @Inject constructor(   
	savedStateHandle: SavedStateHandle,   
	userRepository: UserRepository
) : ViewModel() {   
	val userId : String = savedStateHandle["uid"] ?:          
			throw IllegalArgumentException("missing user id")   
	val user : LiveData<User> = userRepository.getUser(userId)
}
```

## Cache data

The  `UserRepository`  implementation abstracts the call to the  `Webservice`  object, but because it relies on only one data source, it's not very flexible.

The key problem with the  `UserRepository`  implementation is that after it fetches data from our backend, it doesn't store that data anywhere. Therefore, if the user leaves the  `UserProfileFragment`, then returns to it, our app must re-fetch the data, even if it hasn't changed.

This design is suboptimal for the following reasons:

-   It wastes valuable network bandwidth.
-   It forces the user to wait for the new query to complete.

To address these shortcomings, we add a new data source to our  `UserRepository`, which caches the  `User`  objects in memory:

*UserRepository*

```
// Informs Dagger that this class should be constructed only once.
@Singleton
class UserRepository @Inject constructor(   
	private val webservice: Webservice,   
	// Simple in-memory cache. Details omitted for brevity.   
	private val userCache: UserCache
) {   
	fun getUser(userId: String): LiveData<User> {       
		val cached = userCache.get(userId)       
		if (cached != null) {           
			return cached       
		}       
		val data = MutableLiveData<User>()       
		userCache.put(userId, data)       
		// This implementation is still suboptimal but better than before.       
		// A complete implementation also handles error cases.       
		webservice.getUser(userId).enqueue(object : Callback<User> {           
			override fun onResponse(call: Call<User>, response: Response<User>) {               
				data.value = response.body()           
			}           
			// Error case is left out for brevity.           
			override fun onFailure(call: Call<User>, t: Throwable) {               
				TODO()           
			}       
		})       
		return data   
	}
}
```

## Persist data

Using our current implementation, if the user rotates the device or leaves and immediately returns to the app, the existing UI becomes visible instantly because the repository retrieves data from our in-memory cache.

However, what happens if the user leaves the app and comes back hours later, after the Android OS has killed the process? By relying on our current implementation in this situation, we need to fetch the data again from the network. This refetching process isn't just a bad user experience; it's also wasteful because it consumes valuable mobile data.

You could fix this issue by caching the web requests, but that creates a key new problem: What happens if the same user data shows up from another type of request, such as fetching a list of friends? The app would show inconsistent data, which is confusing at best. For example, our app might show two different versions of the same user's data if the user made the list-of-friends request and the single-user request at different times. Our app would need to figure out how to merge this inconsistent data.

The proper way to handle this situation is to use a persistent model. This is where the  [Room](https://developer.android.com/training/data-storage/room/index.html)  persistence library comes to the rescue.

[Room](https://developer.android.com/training/data-storage/room/index.html)  is an object-mapping library that provides local data persistence with minimal boilerplate code. At compile time, it validates each query against your data schema, so broken SQL queries result in compile-time errors instead of runtime failures. Room abstracts away some of the underlying implementation details of working with raw SQL tables and queries. It also allows you to observe changes to the database's data, including collections and join queries, exposing such changes using  _LiveData_  objects. It even explicitly defines execution constraints that address common threading issues, such as accessing storage on the main thread.

**Note:** If your app already uses another persistence solution, such as a SQLite object-relational mapping (ORM), you don't need to replace your existing solution with  [Room](https://developer.android.com/training/data-storage/room/index.html). However, if you're writing a new app or refactoring an existing app, we recommend using Room to persist your app's data. That way, you can take advantage of the library's abstraction and query validation capabilities.

To use Room, we need to define our local schema. First, we add the  [`@Entity`](https://developer.android.com/reference/android/arch/persistence/room/Entity.html)  annotation to our  `User`  data model class and a  [`@PrimaryKey`](https://developer.android.com/reference/android/arch/persistence/room/PrimaryKey)  annotation to the class's  `id`  field. These annotations mark  `User`  as a table in our database and  `id`  as the table's primary key:

User

```
@Entitydata 
class User(   
	@PrimaryKey private val id: String,   
	private val name: String,   
	private val lastName: String
)
```

Then, we create a database class by implementing  [`RoomDatabase`](https://developer.android.com/reference/android/arch/persistence/room/RoomDatabase.html)  for our app:

*UserDatabase*

```
@Database(entities = [User::class], version = 1)
abstract class UserDatabase : RoomDatabase()
```

Notice that  `UserDatabase`  is abstract. Room automatically provides an implementation of it. For details, see the  [Room](https://developer.android.com/training/data-storage/room/)  documentation.

We now need a way to insert user data into the database. For this task, we create a  [data access object (DAO)](https://en.wikipedia.org/wiki/Data_access_object).

*UserDao*

```
@Dao
interface UserDao {   
@Insert(onConflict = REPLACE)   
fun save(user: User)   

@Query("SELECT * FROM user WHERE id = :userId")   
fun load(userId: String): LiveData<User>}
```

Notice that the  `load`  method returns an object of type  `LiveData<User>`. Room knows when the database is modified and automatically notifies all active observers when the data changes. Because Room uses  _LiveData_, this operation is efficient; it updates the data only when there is at least one active observer.

**Note:** Room checks invalidations based on table modifications, which means it may dispatch false positive notifications.

With our  `UserDao`  class defined, we then reference the DAO from our database class:

*UserDatabase*

```
@Database(entities = [User::class], version = 1)
abstract class UserDatabase : RoomDatabase() {   
	abstract fun userDao(): UserDao
}
```

Now we can modify our  `UserRepository`  to incorporate the Room data source:

```
// Informs Dagger that this class should be constructed only once.
@Singleton
class UserRepository @Inject constructor(   
	private val webservice: Webservice,   
	// Simple in-memory cache. Details omitted for brevity.   
	private val executor: Executor,   
	private val userDao: UserDao
) {   
	fun getUser(userId: String): LiveData<User> {       
		refreshUser(userId)       
		// Returns a LiveData object directly from the database.       
		return userDao.load(userId)   
	}   
	
	private fun refreshUser(userId: String) {       
		// Runs in a background thread.       
		executor.execute {           
			// Check if user data was fetched recently.           
			val userExists = userDao.hasUser(FRESH_TIMEOUT)           
			if (!userExists) {               
				// Refreshes the data.               
				val response = webservice.getUser(userId).execute()               
				// Check for errors here.               
				// Updates the database. The LiveData object automatically               
				// refreshes, so we don't need to do anything else here.               
				userDao.save(response.body()!!)           
			}       
		}   
	}   

	companion object {       
		val FRESH_TIMEOUT = TimeUnit.DAYS.toMillis(1)   
	}
}
```

Notice that even though we changed where the data comes from in  `UserRepository`, we didn't need to change our  `UserProfileViewModel`  or  `UserProfileFragment`. This small-scoped update demonstrates the flexibility that our app's architecture provides. It's also great for testing, because we can provide a fake  `UserRepository`  and test our production  `UserProfileViewModel`  at the same time.

If users wait a few days before returning to an app that uses this architecture, it's likely that they'll see out-of-date information until the repository can fetch updated information. Depending on your use case, you may not want to show this out-of-date information. Instead, you can display  _placeholder_  data, which shows dummy values and indicates that your app is currently fetching and loading up-to-date information.

#### Single source of truth

It's common for different REST API endpoints to return the same data. For example, if our backend has another endpoint that returns a list of friends, the same user object could come from two different API endpoints, maybe even using different levels of granularity. If the  `UserRepository`  were to return the response from the  `Webservice`  request as-is, without checking for consistency, our UIs could show confusing information because the version and format of data from the repository would depend on the endpoint most recently called.

For this reason, our  `UserRepository`  implementation saves web service responses into the database. Changes to the database then trigger callbacks on active  _LiveData_  objects. Using this model,  **the database serves as the single source of truth**, and other parts of the app access it using our  `UserRepository`. Regardless of whether you use a disk cache, we recommend that your repository designate a data source as the single source of truth for the rest of your app.

## Show in-progress operations

In some use cases, such as pull-to-refresh, it's important for the UI to show the user that there's currently a network operation in progress. It's good practice to separate the UI action from the actual data because the data might be updated for various reasons. For example, if we fetched a list of friends, the same user might be fetched again programmatically, triggering a  `LiveData<User>`  update. From the UI's perspective, the fact that there's a request in flight is just another data point, similar to any other piece of data in the  `User`  object itself.

We can use one of the following strategies to display a consistent data-updating status in the UI, regardless of where the request to update the data came from:

-   Change  `getUser()`  to return an object of type  `LiveData`. This object would include the status of the network operation.  
    For an example, see the  [`NetworkBoundResource`](https://github.com/googlesamples/android-architecture-components/blob/88747993139224a4bb6dbe985adf652d557de621/GithubBrowserSample/app/src/main/java/com/android/example/github/repository/NetworkBoundResource.kt)  implementation in the android-architecture-components GitHub project.
-   Provide another public function in the  `UserRepository`  class that can return the refresh status of the  `User`. This option is better if you want to show the network status in your UI only when the data-fetching process originated from an explicit user action, such as pull-to-refresh.

## Test each component

In the  [separation of concerns](https://developer.android.com/jetpack/docs/guide#separation-of-concers)  section, we mentioned that one key benefit of following this principle is testability.

The following list shows how to test each code module from our extended example:

-   **User interface and interactions**: Use an  [Android UI instrumentation test](https://developer.android.com/training/testing/unit-testing/instrumented-unit-tests.html). The best way to create this test is to use the  [Espresso](https://developer.android.com/training/testing/ui-testing/espresso-testing.html)  library. You can create the fragment and provide it a mock  `UserProfileViewModel`. Because the fragment communicates only with the  `UserProfileViewModel`, mocking this one class is sufficient to fully test your app's UI.
-   **ViewModel**: You can test the  `UserProfileViewModel`  class using a  [JUnit test](https://developer.android.com/training/testing/unit-testing/local-unit-tests.html). You only need to mock one class,  `UserRepository`.
-   **UserRepository**: You can test the  `UserRepository`  using a JUnit test, as well. You need to mock the  `Webservice`and the  `UserDao`. In these tests, verify the following behavior:
    -   The repository makes the correct web service calls.
    -   The repository saves results into the database.
    -   The repository doesn't make unnecessary requests if the data is cached and up to date.
-   Because both  `Webservice`  and  `UserDao`  are interfaces, you can mock them or create fake implementations for more complex test cases.
-   **UserDao**: Test DAO classes using instrumentation tests. Because these instrumentation tests don't require any UI components, they run quickly. For each test, create an in-memory database to ensure that the test doesn't have any side effects, such as changing the database files on disk.
    
    **Caution:**Room allows specifying the database implementation, so it's possible to test your DAO by providing the JUnit implementation of  [`SupportSQLiteOpenHelper`](https://developer.android.com/reference/android/arch/persistence/db/SupportSQLiteOpenHelper.html). This approach isn't recommended, however, because the SQLite version running on the device might differ from the SQLite version on your development machine.
    
-   **Webservice**: In these tests, avoid making network calls to your backend. It's important for all tests, especially web-based ones, to be independent from the outside world. Several libraries, including  [MockWebServer](https://github.com/square/okhttp/tree/master/mockwebserver), can help you create a fake local server for these tests.
    
-   **Testing Artifacts**: Architecture Components provides a maven artifact to control its background threads. The  `androidx.arch.core:core-testing`  artifact contains the following JUnit rules:
    
    -   `InstantTaskExecutorRule`: Use this rule to instantly execute any background operation on the calling thread.
    -   `CountingTaskExecutorRule`: Use this rule to wait on background operations of Architecture Components. You can also associate this rule with Espresso as an  [idling resource](https://developer.android.com/training/testing/espresso/idling-resource).
